<?php
use ricco\ticket\models\TicketHead;
?>
<p style="text-align: center;"><img src="http://wallbtc.com/img/logo.png" alt=""/></p>
<p style="text-align: left; font-size: 14px;"><?= \yii\bootstrap\Html::encode($textTicket)?></p>
<hr/>
<p>
    <strong>Тикет:&nbsp;</strong><?=$nameTicket?>
    <br/>
    <strong>Статус:&nbsp;</strong>
    <?php
    switch ($status) {
        case TicketHead::OPEN :
            echo 'Opened';break;
        case TicketHead::WAIT :
            echo 'Waiting';break;
        case TicketHead::ANSWER :
            echo 'Solved';break;
        case TicketHead::CLOSED :
            echo 'Closed';break;
    }
    ?>
    <br/>
    <strong>Link:&nbsp;
        <a
            href="<?=$link?>"><?=$link?>
        </a>
    </strong>
</p>
<hr/>
<em>
    <span style="color: #808080;">This email is generated automatically by the notification service. You do not need to respond to it.</span>
</em