<?php

namespace ricco\ticket;

use ricco\ticket\models\User;
use Yii;

/**
 * ticket module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'ricco\ticket\controllers';

    /** @var bool Уведомление на почту о тикетах */
    public $mailSend = false;

    /** @var string Тема email сообщения когда пользователю приходит Answer */
    public $subjectAnswer = 'Answer on the exemple.com site';

    /** @var  User */
    public $userModel = false;

    public $qq = [
        'Awesome'   => 'Awesome',
        'Complaints'   => 'Complaints',
        'Other'        => 'Other',
    ];

    /** @var array Ники Adminав */
    public $admin = ['admin'];
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        User::$user = ($this->userModel !== false) ? $this->userModel : Yii::$app->user->identityClass;
        parent::init();
    }
}
