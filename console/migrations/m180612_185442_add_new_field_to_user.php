<?php

use yii\db\Migration;

/**
 * Class m180612_185442_add_new_field_to_user
 */
class m180612_185442_add_new_field_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180612_185442_add_new_field_to_user cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->addColumn('{{%profile}}', 'company', $this->string(255));
        $this->addColumn('{{%profile}}', 'job', $this->string(255));
    }

    public function down()
    {
        $this->addColumn('{{%profile}}', 'company');
        $this->addColumn('{{%profile}}', 'job');
    }
}
