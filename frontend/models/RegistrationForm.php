<?php

namespace common\modules\user\models;

use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use dektrium\user\models\User;

class RegistrationForm extends BaseRegistrationForm
{
    /**
     * @var string First Name
     */
    public $firstName;

    /**
     * @var string Last Name
     */
    public $lastName;

    /**
     * @var string Company Name
     */
    public $company;

    /**
     * @var string Job Title
     */
    public $jobTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['firstName', 'required'];
        $rules[] = ['firstName', 'string', 'max' => 45];

        $rules[] = ['lastName', 'required'];
        $rules[] = ['lastName', 'string', 'max' => 45];

        $rules[] = ['company', 'required'];
        $rules[] = ['company', 'string', 'max' => 255];

        $rules[] = ['jobTitle', 'required'];
        $rules[] = ['jobTitle', 'string', 'max' => 255];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['firstName'] = \Yii::t('user', 'First Name');
        $labels['lastName']  = \Yii::t('user', 'Last Name');
        $labels['company']   = \Yii::t('user', 'Company');
        $labels['jobTitle']  = \Yii::t('user', 'Job Title');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(User $user)
    {
        // here is the magic happens
        $user->setAttributes([
            'email'    => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ]);
        /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'company' => $this->company,
            'jobTitle' => $this->jobTitle,
        ]);
        $user->setProfile($profile);
    }
}