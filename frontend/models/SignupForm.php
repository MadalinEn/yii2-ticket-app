<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $firstName;
    public $lastName;
    public $company;
    public $jobTitle;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['firstName', 'trim'],
            ['firstName', 'required'],
            ['firstName', 'string', 'max' => 35],

            ['lastName', 'trim'],
            ['lastName', 'required'],
            ['lastName', 'string', 'max' => 35],

            ['company', 'trim'],
            ['company', 'required'],
            ['company', 'string', 'max' => 35],

            ['jobTitle', 'trim'],
            ['jobTitle', 'required'],
            ['jobTitle', 'string', 'max' => 35],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->first_name = $this->firstName;
        $user->last_name = $this->lastName;
        $user->company = $this->company;
        $user->job_title = $this->jobTitle;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
