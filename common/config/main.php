<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin','editor','user'], // here define your roles
            'itemFile' => '@console/data/items.php', //Default path to items.php | NEW CONFIGURATIONS
            'assignmentFile' => '@console/data/assignments.php', //Default path to assignments.php | NEW CONFIGURATIONS
            'ruleFile' => '@console/data/rules.php', //Default path to rules.php | NEW CONFIGURATIONS
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => 'common/views/user'
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
            'admins' => ['madalinen'],
            'modelMap' => [
                'RegistrationForm' => 'common\models\RegistrationForm',  
                'Profile' => 'common\models\Profile', 
            ] ,
        ],
        'ticket' => [
            'class' => 'ricco\ticket\Module',
        ],
    ],
];
