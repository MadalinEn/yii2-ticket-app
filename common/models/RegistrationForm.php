<?php

namespace common\models;

use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use dektrium\user\models\User;

class RegistrationForm extends BaseRegistrationForm
{
    /**
     * @var string Name
     */
    public $name;

    /**
     * @var string Company Name
     */
    public $company;

    /**
     * @var string Job Title
     */
    public $job;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['name', 'required'];
        $rules[] = ['name', 'string', 'max' => 255];

        $rules[] = ['company', 'required'];
        $rules[] = ['company', 'string', 'max' => 255];

        $rules[] = ['job', 'required'];
        $rules[] = ['job', 'string', 'max' => 255];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = \Yii::t('user', 'Full Name');
        $labels['company']   = \Yii::t('user', 'Company');
        $labels['job']  = \Yii::t('user', 'Job Title');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(User $user)
    {
        // here is the magic happens
        $user->setAttributes([
            'email'     => $this->email,
            'username'  => $this->username,
            'password'  => $this->password,
        ]);
        /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'name'    => $this->name,
            'company' => $this->company,
            'job'     => $this->job,
        ]);
        $user->setProfile($profile);
    }
}