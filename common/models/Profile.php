<?php

namespace common\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{
	public function rules()
    {
        $rules = parent::rules();

        $rules['companyRequired'] = ['company', 'required'];
        $rules['companyLength']   = ['company', 'string', 'max' => 255];
        
        $rules['jobRequired'] = ['job', 'required'];
        $rules['jobLength']   = ['job', 'string', 'max' => 255];

        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        
        $attributeLabels['company'] = \Yii::t('user', 'Company');
        $attributeLabels['job'] = \Yii::t('user', 'Job Title');

        return $attributeLabels;
    }
}